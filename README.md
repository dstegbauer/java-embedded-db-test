# Performance test of java embedded databases

## Description

This is (for now) very simple test of performance of various embedded java databases:

* SQLite

## How to run

### Test SQLite

```shell
./gradlew run --args sqlite
```

### Test H2

```shell
./gradlew run --args h2
```
