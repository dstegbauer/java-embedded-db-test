package bau.steg.dbtest

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands

private class App : CliktCommand() {
    override fun run() {
        // nothing to do here
    }
}

fun main(args: Array<String>) = App()
    .subcommands(
        SQLitePerfTest(),
        H2PerfTest(),
    )
    .main(args)
