package bau.steg.dbtest

import com.github.ajalt.clikt.core.CliktCommand
import java.io.File
import java.sql.DriverManager

class SQLitePerfTest : CliktCommand(
    name = "sqlite",
    help = "SQLite performance test",
) {
    companion object {
        const val storageLocation = "/tmp/dbtest.sqlite"
    }
    override fun run() {
        with(File(storageLocation)) {
            if (isFile) {
                delete()
            }
        }
        DriverManager.getConnection("jdbc:sqlite:$storageLocation").use { connection ->
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS orders (id integer PRIMARY KEY, price integer NOT NULL, quantity integer NOT NULL);")
            connection.autoCommit = false
            connection.beginRequest()
            val insertStmt = connection.prepareStatement("INSERT INTO orders (id, price, quantity) VALUES (?, ?, ?)")
            val start = System.currentTimeMillis()
            repeat(10_000_000) { idx ->
                insertStmt.setLong(1, idx.toLong())
                insertStmt.setLong(2, idx * 1000L)
                insertStmt.setLong(3, idx * 10000L)
                insertStmt.execute()
                insertStmt.clearParameters()
            }
            connection.commit()
            val end = System.currentTimeMillis()
            println("SQLite duration ${(end - start) / 1000.0}")
        }
    }
}
