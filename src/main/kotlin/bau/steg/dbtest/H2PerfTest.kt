package bau.steg.dbtest

import com.github.ajalt.clikt.core.CliktCommand
import java.io.File
import java.sql.DriverManager

class H2PerfTest : CliktCommand(
    name = "h2",
    help = "h2 producer",
) {
    companion object {
        const val storageLocation = "/tmp/dbtest.h2"
    }
    override fun run() {
        with(File(SQLitePerfTest.storageLocation)) {
            if (isFile) {
                delete()
            }
        }

        val props = mapOf(
            "user" to "test",
            "password" to "test"
        ).toProperties()
        DriverManager.getConnection("jdbc:h2:${storageLocation}", props).use { connection ->
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS orders (id integer PRIMARY KEY, price integer NOT NULL, quantity integer NOT NULL);")
            connection.autoCommit = false
            val start = System.currentTimeMillis()
            connection.beginRequest()
            val insertStmt = connection.prepareStatement("INSERT INTO orders (id, price, quantity) VALUES (?, ?, ?)")
            repeat(10_000_000) { idx ->
                insertStmt.setInt(1, idx)
                insertStmt.setInt(2, idx * 1000)
                insertStmt.setInt(3, idx * 10000)
                insertStmt.execute()
                insertStmt.clearParameters()
            }
            connection.commit()
            val end = System.currentTimeMillis()
            println("H2 duration ${(end - start) / 1000.0}")
        }
    }
}
