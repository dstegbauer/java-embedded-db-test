#!/bin/bash

./gradlew useLatestVersions --refresh-dependencies --max-workers 1
updateGradle=$(jq .gradle.current.isUpdateAvailable build/dependencyUpdates/report.json)
if [ "$updateGradle" == 'true' ] ; then
  gradleNewVersion=$(jq .gradle.current.version build/dependencyUpdates/report.json)
  ./gradlew wrapper --max-workers 1
fi
